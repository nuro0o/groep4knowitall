<?php
// Create database connection
$db = mysqli_connect("localhost", "user1", "User1", "knowitall");

// Initialize message variable
$msg = "";
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location: login.php");
    exit;
}
// If upload button is clicked ...
if (isset($_POST['upload'])) {
    // Get image name
    $path_parts = pathinfo($_FILES["image"]["name"]);

    $image = md5(uniqid($_FILES['image']['name'], true)) . '.' .  $path_parts['extension'];
    // Get text
    $image_text = mysqli_real_escape_string($db, $_POST['text']);
    $gametext = mysqli_real_escape_string($db, $_POST['gametext']);
    $username = mysqli_real_escape_string($db, $_SESSION['username']);

    // image file directory
    $target = "images/".basename($image);

    $sql = "INSERT INTO weetjes (image, title, gametext, username) 
            VALUES ('$image', '$image_text','$gametext','$username')";
    // execute query
    mysqli_query($db, $sql);

    if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
        $msg = "Image uploaded successfully";
    }else{
        $msg = "Failed to upload image";
    }
}
$maxposts = 0;
$result = mysqli_query($db, "SELECT * FROM weetjes ORDER BY date desc");
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/main.css">
    <title>Foto Uploaden</title>
    <style type="text/css">
        img{
            width: 300px;
            height: 300px;
        }
    </style>
</head>
<body background="images/background.jpg">
<div class="maingrid3">
    <?php
    while ($row = mysqli_fetch_array($result)) {
        echo "<div class='grid-itempost'>";
        echo "<p>".$row['title']."</p>";
        echo "<img src='images/".$row['image']."' >";
        echo "</div>";
        echo "<div class='grid-item'>";
        echo "<p>".$row['username']."</p>";
        echo "<p>".$row['gametext']."</p>";
        echo "</div>";
        if($maxposts++ ==9)break;
    }
    ?>
</div>
<div class="maingrid">
    <div class="grid-item">
    <form method="POST" action="" enctype="multipart/form-data">
        <input type="hidden" name="size" value="1000000">
        <div>
            <input type="file" name="image">
        </div>
        <div>
      <input type="text" name="text" placeholder="Titel">
        </div>
        <div>
      <textarea id="text" cols="10" rows="4" name="gametext" placeholder="Game Descriptie"></textarea>
        </div>
        <div class="grid-itemcolor">
        <div class="postknop">
            <input type="submit" name="upload" value="Posten">
            <a href="indexlogin.php" class="terug">Terug</a>
        </div>
        </div>
    </form>
    </div>
</div>
</body>
</html>