<?php
$db = mysqli_connect("localhost", "user1", "User1", "knowitall");

$result = mysqli_query($db, "SELECT * FROM weetjes ORDER BY date desc");
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location: login.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        img{
            width: 300px;
            height: 300px;
        }
    </style>
</head>

<body background="images/background.jpg">
<div class="maingridcolor">
<div class="maingrid">
    <div class="grid-itemlogin">
        <a href="logout.php"">Uitloggen</a>
        <a href="post.php">Posten</a>
    </div>
</div>
<div class="maingrid">
    <div class="grid-itemcolor">
        <h1>Gamewiki KnowItAll</h1>
        <div class="searchbutton">
            <form action="searchloggedin.php" method="POST" enctype="multipart/form-data">
                <input class="search" type="text" name="search" placeholder="Search..">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <h1>Hoi, <b><?php echo htmlspecialchars($_SESSION['username']); ?></b>. Welkom op onze site.</h1>
        <h1>Nieuwste Weetjes</h1>
    </div>
</div>
<div class="maingrid2">
    <div class="picturegridcolor">
        <?php
        while ($row = mysqli_fetch_array($result)) {
            echo "<img src='images/".$row['image']."' >";
            break;
        }
        ?>
    </div>
    <div class="grid-itemcolor">
        <?php
        while ($row = mysqli_fetch_array($result)) {
            echo "<p>".'Gebruiker: '.$row['username']."</p>";
            echo "<p>".'Titel: '.$row['title']."</p>";
            echo "<p>".'Descriptie: '.$row['gametext']."</p>";
            break;
        }
        ?>
    </div>
    <div class="picturegridcolor">
        <?php
        while ($row = mysqli_fetch_array($result)) {
            echo "<img src='images/".$row['image']."' >";
            break;
        }
        ?>
    </div>
    <div class="grid-itemcolor">
        <?php
        while ($row = mysqli_fetch_array($result)) {
            echo "<p>".'Gebruiker: '.$row['username']."</p>";
            echo "<p>".'Titel: '.$row['title']."</p>";
            echo "<p>".'Descriptie: '.$row['gametext']."</p>";
            break;
        }
        ?>
    </div>
</div>
    <footer>
        <div class="grid-itemfootercolor">
            <a href="faqloggedin.php">FAQ</a>
            <a href="aboutusloggedin.php">Over Ons</a>
            <a href="contactloggedin.php">Contact</a>
        </div>
    </footer>
</div>
</body>

</html>